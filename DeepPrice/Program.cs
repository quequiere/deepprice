﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeepPrice;
using DeepPrice.models;
using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Transforms.Categorical;
using Microsoft.ML.Transforms.Projections;
using PLplot;

namespace DeepPrice
{
    class Program
    {
        static PLStream pls = new PLStream();

        static  int XPTS = 35;
        static  int YPTS = 45;



        static double[] alt = { 60.0, 40.0 };
        static double[] az = { 30.0, -30.0 };

        static void Main(string[] args)
        {
            List<Candle> liste = JsonHelper.loadData(@".\assets\ChartData.json");


            pls = new PLStream();

            pls.sdev("pngcairo");
            pls.sfnam("SineWaves.png");

            pls.spal0("cmap0_alternate.pal");
            pls.init();




            pls.adv(0);
            pls.col0(1);
            pls.vpor(0.0, 1.0, 0.0, 1);

            pls.wind(-1.0, 1.0, -1.0, 1.5);

            double center = 0.02;

            pls.w3d(1.0, 1.0, 1.2, -center, center, -center, center, -center, center, 30, 30);


            pls.box3("bnstu", "x axis", 0.0, 0,"bnstu", "y axis", 0.0, 0, "bcdmnstuv", "z axis", 0.0, 1);



            pls.lab("X", "Y", "Candle repartition");


            //double[] corpCandle = liste.Select(c => (c.open / c.close)-1).ToArray();
            //double[] mecheCandle = liste.Select(c => (c.open / c.close)-1).ToArray();

            //pl.poin(corpCandle,mecheCandle,'.');



            pls.scmap1n(256);
      
            pls.mtex("t", 1.0, 0.5, 0.5, "Candle");


            double[] xa = new double[liste.Count];
            double[] ya = new double[liste.Count];
            double[] za = new double[liste.Count];

            int scan = 0;

            List<String> csvOut = new List<string>();

            foreach (Candle c in liste)
            {
                xa[scan] = (c.close / c.open) - 1;

                ya[scan] = (c.max / c.open) - 1;

                za[scan] = (c.min / c.open) - 1;

                csvOut.Add($"{xa[scan]},{ya[scan]},{za[scan]}");

                scan++;
            }

            System.IO.File.WriteAllLines("output.csv", csvOut);

            pls.poin3(xa, ya, za, '.');
            pls.eop();
            pls.gver(out _);


            pls.eop();

            pls.gver(out _);


            MLContext mlContext = new MLContext(seed: 1);

            // STEP 1: Common data loading configuration
            TextLoader textLoader = mlContext.Data.CreateTextReader(
                            columns: new[]
                                        {
                                        new TextLoader.Column("Features", DataKind.R4, new[] {new TextLoader.Range(0, 31) }),
                                        new TextLoader.Column("LastName", DataKind.Text, 32)
                                        },
                            hasHeader: true,
                            separatorChar: ',');

            var pivotDataView = textLoader.Read("output.csv");

            var dataProcessPipeline = new PrincipalComponentAnalysisEstimator(mlContext, "Features", "PCAFeatures", rank: 2)
                                .Append(new OneHotEncodingEstimator(mlContext, new[] { new OneHotEncodingEstimator.ColumnInfo("LastName",
                                                                   "LastNameKey",
                                                                   OneHotEncodingTransformer.OutputKind.Ind) }));

            var trainer = mlContext.Clustering.Trainers.KMeans("Features", clustersCount: 2);
            var trainingPipeline = dataProcessPipeline.Append(trainer);



            Console.ReadLine();

        }



    }
}
