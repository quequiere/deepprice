﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Linq;
using DeepPrice.models;

namespace DeepPrice
{
    public class JsonHelper
    {
        private static string read(string path)
        {
            string text = System.IO.File.ReadAllText(path);
            return text;
        }

        public static List<Candle> loadData(string path)
        {
            string dataInput = read(path);

            dynamic json = JObject.Parse(dataInput);

            JArray data = json.content.panes[0].sources[0].bars.data;

            List<Candle> liste = new List<Candle>();

            foreach (JToken d in data)
            {
                JArray v = d["value"] as JArray;

                long time = v[0].Value<long>();
                double open = v[1].Value<double>();
                double close = v[4].Value<double>();
                double min = v[3].Value<double>();
                double max = v[2].Value<double>();

                Candle c = new Candle(time,open,close,max,min);
                liste.Add(c);
            }

            return liste;

        }
    }
}
