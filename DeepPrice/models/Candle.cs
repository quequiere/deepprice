﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeepPrice.models
{
    public class Candle
    {
        public long time { get; }
        public double open { get; }
        public double close { get; }
        public double max { get; }
        public double min { get; }

        public Candle(long time, double open, double close, double maxHigh, double minHigh)
        {
            this.time = time;
            this.open = open;
            this.close = close;
            this.max = maxHigh;
            this.min = minHigh;
        }

        public override string ToString()
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            DateTime dt = origin.AddSeconds(time);

            string str = dt.ToString("dd/MM/yyyy HH:mm");

            return $"{str} OC at:{open} --> {close} with MinMax {min} --> {max}";
        }
    }
}
